/**
 * JS file that supports the domain_bv module
 */
(function ($) {
  Drupal.behaviors.domain_bv = {
    attach: function (context, settings) {
      $('input.did-domain_site').click(function(){
        // Get value of checked checkbox.
        var $value = 0;
        if ($(this).is(':checked')) {
          $value = 1;
        }

        // If checkbox for global is unchecked, select all domains.
        // If checkbox for global is checked, deselect all domains.
        if ($value == 0) {
          $(this).closest('tr').find( "input[type='checkbox']" ).not(".did-domain_site").attr('checked', true);
        }
        else {
          $(this).closest('tr').find( "input[type='checkbox']" ).not(".did-domain_site").attr('checked', false);
        }
      });
    }
  };
}(jQuery));