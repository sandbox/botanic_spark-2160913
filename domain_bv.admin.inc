<?php
/* *
 * @file
 * Set of functions required by Domain blocks visibility module.
 */

/**
 * Callback for domain_bv configuration page.
 */
function domain_bv_configure($theme = NULL, $region = NULL) {
  global $theme_key;

  if (!isset($theme)) {
    // If theme is not specifically set, rehash for the current theme.
    $theme = $theme_key;
  }

  module_load_include('inc', 'block', 'block.admin');

  // Get list of all blocks.
  $blocks = block_admin_display_prepare_blocks($theme);

  // If region is set, return blocks only for that region.
  if (isset($region)) {
    $region_key = $region;
    $region = array();

    // Get list of all regions. Get current region.
    $block_regions = system_region_list($theme, REGIONS_VISIBLE);
    $region[$region_key] = $block_regions[$region_key];

    // Get only blocks for given region.
    $blocks_region = array();
    foreach ($blocks as $key => $block) {
      if ($block['region'] == 'top_user') {
        $blocks_region[] = $block;
      }
    }

    // Pass the list of blocks for given region.
    if (!empty($blocks_region)) {
      $blocks = $blocks_region;
    }
  }

  return drupal_get_form('domain_bv_admin_display_form', $blocks, $theme, $region);
}

/**
 * Form constructor for the main block_bv administration form.
 *
 * @param $blocks
 *   An array of blocks, as returned by block_admin_display_prepare_blocks().
 * @param $theme
 *   A string representing the name of the theme to edit blocks visibility for.
 * @param $block_regions
 *   (optional) An array of regions in which the blocks will be allowed to be
 *   placed. Defaults to all visible regions for the theme whose blocks are
 *   being configured. In all cases, a dummy region for disabled blocks will
 *   also be displayed.
 *
 * @return
 *   An array representing the form definition.
 *
 * @ingroup forms
 * @see block_admin_display_form_submit()
 */
function domain_bv_admin_display_form($form, &$form_state, $blocks, $theme, $block_regions = NULL) {

  $form['#attached']['css'] = array(drupal_get_path('module', 'block') . '/block.css');
  $form['#attached']['js'] = array(drupal_get_path('module', 'domain_bv') . '/js/domain_bv.js');

  // Get a list of block regions if one was not provided.
  if (!isset($block_regions)) {
    $block_regions = system_region_list($theme, REGIONS_VISIBLE);
    // Add a last region for disabled blocks.
    $block_regions = $block_regions + array(BLOCK_REGION_NONE => BLOCK_REGION_NONE);
  }

  $domains = domain_domains();
  $form['block_domains'] = array(
    '#type' => 'value',
    '#value' => $domains,
  );

  // Weights range from -delta to +delta, so delta should be at least half
  // of the amount of blocks present. This makes sure all blocks in the same
  // region get an unique weight.
  $weight_delta = round(count($blocks) / 2);

  // Build the form tree.
  $form['edited_theme'] = array(
    '#type' => 'value',
    '#value' => $theme,
  );
  $form['block_regions'] = array(
    '#type' => 'value',
    '#value' => $block_regions,
  );
  $form['blocks'] = array();
  $form['#tree'] = TRUE;

  // Get list of domains for current block.
  $domain_blocks = _domain_bv_blocks_load();

  foreach ($blocks as $i => $block) {
    $key = $block['module'] . '_' . $block['delta'];
    $form['blocks'][$key]['module'] = array(
      '#type' => 'value',
      '#value' => $block['module'],
    );
    $form['blocks'][$key]['delta'] = array(
      '#type' => 'value',
      '#value' => $block['delta'],
    );

    $form['blocks'][$key]['info'] = array(
      '#markup' => check_plain($block['info']),
    );
    $form['blocks'][$key]['theme'] = array(
      '#type' => 'hidden',
      '#value' => $theme,
    );
    $form['blocks'][$key]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $block['weight'],
      '#delta' => $weight_delta,
      '#title_display' => 'invisible',
      '#title' => t('Weight for @block block', array('@block' => $block['info'])),
    );
    $form['blocks'][$key]['region'] = array(
      '#type' => 'hidden',
      '#default_value' => $block['region'] != BLOCK_REGION_NONE ? $block['region'] : NULL,
    );
    if ($block['module'] == 'block') {
      $form['blocks'][$key]['delete'] = array(
        '#type' => 'link',
        '#title' => t('delete'),
        '#href' => 'admin/structure/block/manage/' . $block['module'] . '/' . $block['delta'] . '/delete',
      );
    }

    // Get list of domains for current block.
    $domain_block = _domain_bv_get_block_info($block['module'], $block['delta'], $domain_blocks);

    // Checkbox for selecting all domains.
    $domain_default_value_global = (!empty($domain_block) && in_array('domain_site', $domain_block)) ? 1 : 0;
    $form['blocks'][$key]['domains']['domain_site'] = array(
      '#type' => 'checkbox',
      '#default_value' => $domain_default_value_global,
      '#attributes' => array('class' => array('did-domain_site')),
    );

    // Checkboxes for choosing separate domains.
    foreach ($domains as $did => $domain) {
      $domain_default_value = (!empty($domain_block) && in_array($domain['domain_id'], $domain_block)) ? 1 : 0;
      $form['blocks'][$key]['domains'][$domain['domain_id']] = array(
        '#type' => 'checkbox',
        '#default_value' => $domain_default_value,
        '#attributes' => array('class' => array('did-' . $domain['domain_id'])),
      );
    }
  }
  // Do not allow disabling the main system content block when it is present.
  if (isset($form['blocks']['system_main']['region'])) {
    $form['blocks']['system_main']['region']['#required'] = TRUE;
  }

  $form['actions'] = array(
    '#tree' => FALSE,
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  return $form;
}

/**
 * Form submission handler for domain_bv_admin_display_form().
 *
 * @see block_admin_display_form()
 */
function domain_bv_admin_display_form_submit($form, &$form_state) {
  $transaction = db_transaction();
  try {
    // Delete all entries from domain_blocks table.
    db_delete('domain_blocks')->execute();

    // Iterate through values and insert data in database.
    foreach ($form_state['values']['blocks'] as $block) {
      foreach ($block['domains'] as $key => $domain) {
        // Save blocks with global visibility.
        if ($key == 'domain_site' && $domain == 1) {
          $id = db_insert('domain_blocks')
            ->fields(array(
              'module' => $block['module'],
              'delta' => $block['delta'],
              'realm' => 'domain_site',
              'domain_id' => 0,
            ))
            ->execute();
        }
        // Save seaparate domain.
        elseif ($domain != 0) {
          $id = db_insert('domain_blocks')
            ->fields(array(
              'module' => $block['module'],
              'delta' => $block['delta'],
              'realm' => 'domain_id',
              'domain_id' => $key,
            ))
            ->execute();
        }
      }
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('block', $e);
    throw $e;
  }
  drupal_set_message(t('The block settings have been updated.'));
}

/**
 * Processes variables for block-admin-display-form.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $form
 *
 * @see block-admin-display.tpl.php
 * @see theme_block_admin_display()
 */
function template_preprocess_domain_bv_admin_display_form(&$variables) {
  $variables['block_domains'] = $variables['form']['block_domains']['#value'];
  $variables['block_regions'] = $variables['form']['block_regions']['#value'];
  if (isset($variables['block_regions'][BLOCK_REGION_NONE])) {
    $variables['block_regions'][BLOCK_REGION_NONE] = t('Disabled');
  }

  foreach ($variables['block_regions'] as $key => $value) {
    // Initialize an empty array for the region.
    $variables['block_listing'][$key] = array();
  }

  // Initialize disabled blocks array.
  $variables['block_listing'][BLOCK_REGION_NONE] = array();


  // Add each block in the form to the appropriate place in the block listing.
  foreach (element_children($variables['form']['blocks']) as $i) {
    $block = &$variables['form']['blocks'][$i];

    // Fetch the region for the current block.
    $region = (isset($block['region']['#default_value']) ? $block['region']['#default_value'] : BLOCK_REGION_NONE);

    // Set special classes needed for table drag and drop.
    $block['region']['#attributes']['class'] = array('block-region-select', 'block-region-' . $region);
    $block['weight']['#attributes']['class'] = array('block-weight', 'block-weight-' . $region);

    $variables['block_listing'][$region][$i] = new stdClass();
    $variables['block_listing'][$region][$i]->row_class = !empty($block['#attributes']['class']) ? implode(' ', $block['#attributes']['class']) : '';
    $variables['block_listing'][$region][$i]->block_modified = !empty($block['#attributes']['class']) && in_array('block-modified', $block['#attributes']['class']);
    $variables['block_listing'][$region][$i]->block_title = drupal_render($block['info']);
    $variables['block_listing'][$region][$i]->region_select = drupal_render($block['region']) . drupal_render($block['theme']);
    $variables['block_listing'][$region][$i]->weight_select = drupal_render($block['weight']);
    $variables['block_listing'][$region][$i]->configure_link = drupal_render($block['configure']);
    $variables['block_listing'][$region][$i]->delete_link = !empty($block['delete']) ? drupal_render($block['delete']) : '';
    $variables['block_listing'][$region][$i]->domains = $block['domains'];
    $variables['block_listing'][$region][$i]->printed = FALSE;
    hide($block['domains']);
  }

  $variables['form_submit'] = drupal_render_children($variables['form']);
}

/**
 * Returns block visibility info for current block.
 *
 * FALSE if not available, array('domain_site') if using domain_site grant,
 * keyed array if using domain_id grant(s).
 *
 * @param $module
 * @param $delta
 * @param $blocks
 * @return array|bool
 */
function _domain_bv_get_block_info($module, $delta, $blocks) {
  if (isset($blocks[$module . '_' . $delta])) {
    $block = $blocks[$module . '_' . $delta];
    if (isset($block['domain_site'])) {
      return array('domain_site');
    }
    elseif (!empty($block)) {
      return $block['domain_id'];
    }
  }
  return FALSE;
}

/**
 * Get list of all blocks from domain_blocks table.
 *
 * @return array|null
 */
function _domain_bv_blocks_load() {
  $block = NULL;
  $query = db_select('domain_blocks', 'db')
    ->fields('db', array());
  $result = $query->execute()->fetchAll();

  foreach ($result as $record) {
    $block[$record->module . '_' . $record->delta][$record->realm][] = $record->domain_id;
  }
  return $block;
}